import json
import os.path

from tensorflow.examples.tutorials.mnist import input_data

import numpy as np
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score, precision_score, f1_score, recall_score
import sklearn.neighbors as neighbors
import matplotlib.pyplot as plt
import seaborn as sns

sns.set()

mnist_data = input_data.read_data_sets("/hdd/erez/mnist")
tr_images = mnist_data.train.images
tr_labels = mnist_data.train.labels
te_images = mnist_data.test.images
te_labels = mnist_data.test.labels

CACHE_FILE_NAME = 'cache.txt'
CACHED_ORIGINAL_NAME = 1.


class AsDictionaryEncoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__


class FitMetrics:

    def __init__(self, y_true, y_pred, percentage, dimension):
        self.dimension = dimension
        self.percentage = percentage

        self.accuracy = 0
        self.recall = 0
        self.precision = 0
        self.f1_score = 0

        if y_true is 0:
            return

        self.accuracy = accuracy_score(y_true, y_pred)
        self.recall = recall_score(y_true, y_pred, average='macro')
        self.precision = precision_score(y_true, y_pred, average='macro')
        self.f1_score = f1_score(y_true, y_pred, average='macro')

    def __repr__(self):
        return "percentage = {}%  |  dim = {}  |||  accuracy = {}  |  recall = {}  |  precision = {}  |  f1 = {}" \
            .format(self.percentage, self.dimension, self.accuracy, self.recall, self.precision, self.f1_score)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    @staticmethod
    def fromJSON(json_string):
        dict = json.loads(json_string)
        return FitMetrics.from_dictionary(dict)

    @staticmethod
    def from_dictionary(dict):
        metrics = FitMetrics(0, 0, 0, 0)

        for k in metrics.__dict__:
            metrics.__dict__[k] = dict[k]

        return metrics

    def to_ndarray(self):
        return np.array([self.accuracy, self.precision, self.recall, self.f1_score, self.dimension])


if __name__ == "__main__":

    metrics = {}


    # Added caching mechanism because KNN is tedious
    def update_metrics_cache():
        with open(CACHE_FILE_NAME, mode='w') as f:
            json.dump(metrics, f, cls=AsDictionaryEncoder)


    if os.path.isfile(CACHE_FILE_NAME):
        with open(CACHE_FILE_NAME, mode='r') as f:
            loaded = json.load(f)

        for metric_percentage in loaded:
            metric = FitMetrics.from_dictionary(loaded[metric_percentage])
            metrics[float(metric_percentage)] = metric
    else:
        update_metrics_cache()

    knn = neighbors.KNeighborsClassifier(n_jobs=- 1)

    if CACHED_ORIGINAL_NAME not in metrics:
        knn.fit(tr_images, tr_labels)

        metrics_full = FitMetrics(te_labels, knn.predict(te_images), CACHED_ORIGINAL_NAME, tr_images.shape[1])
        metrics[CACHED_ORIGINAL_NAME] = metrics_full
        update_metrics_cache()
        print(metrics_full)

    percentages = np.arange(980, 250, -20) / 1000
    for percentage in percentages:
        if percentage in metrics:
            continue

        pca = PCA(svd_solver="full", n_components=percentage)
        knn.fit(pca.fit_transform(tr_images), tr_labels)

        predicted_labels = knn.predict(pca.transform(te_images))
        metric = FitMetrics(te_labels, predicted_labels, percentage, pca.components_.shape[0])
        metrics[percentage] = metric
        update_metrics_cache()
        print(metric)

    plt.figure()

    x_axis = sorted([k for k in metrics.keys()])
    y_axes = np.empty((5, len(x_axis)))
    for i, x in enumerate(x_axis):
        y_axes[:, i] = metrics[x].to_ndarray()

    labels = ['Dimension %', 'F1 score', 'Recall', 'Precision', 'Accuracy']
    y_axes[4] = y_axes[4] /max(y_axes[4])
    handles = []
    for y in range(y_axes.shape[0]):
        handles.append(plt.plot(x_axis, y_axes[y], '-x', label = labels.pop()))

    plt.gca().invert_xaxis()
    plt.legend()
    plt.show()
